# echo "Waiting for Kafka to come online..."

cub kafka-ready -b kafka:9092 1 20

# create the input topic
kafka-topics \
  --bootstrap-server kafka:9092 \
  --topic input \
  --replication-factor 1 \
  --partitions 3 \
  --create

# create the output topic
kafka-topics \
  --bootstrap-server kafka:9092 \
  --topic output \
  --replication-factor 1 \
  --partitions 3 \
  --create

#sleep infinity

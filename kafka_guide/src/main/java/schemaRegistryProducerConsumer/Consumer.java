package schemaRegistryProducerConsumer;

import domain.avro.CustomerAvro;
import io.confluent.kafka.serializers.KafkaAvroDeserializer;
import io.confluent.kafka.serializers.KafkaAvroDeserializerConfig;
import org.apache.kafka.clients.consumer.*;
import org.apache.kafka.common.TopicPartition;

import java.time.Duration;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Consumer {
    private static final int CUSTOMER_QUANTITY = 1000;
    private static final String TOPIC = "input";

    public static void main(String[] args) {
        Properties props = new Properties();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:29092");
        props.put(ConsumerConfig.GROUP_ID_CONFIG, "coffee.consumer7");
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, KafkaAvroDeserializer.class.getName());
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, KafkaAvroDeserializer.class.getName());
        props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, false);
//        props.put(ConsumerConfig.AUTO_COMMIT_INTERVAL_MS_CONFIG, 10);
        props.put(KafkaAvroDeserializerConfig.SCHEMA_REGISTRY_URL_CONFIG, "http://localhost:8081");
        props.put(KafkaAvroDeserializerConfig.SPECIFIC_AVRO_READER_CONFIG, true);  // !!! important, else GenericType

        int availableProcessors = Runtime.getRuntime().availableProcessors();
        System.out.println("available processors = " + availableProcessors);

        ExecutorService executorService = Executors.newFixedThreadPool(availableProcessors);

        executorService.submit(new Tasks(props, TOPIC, "consumer_" + 1));
        executorService.submit(new Tasks(props, TOPIC, "consumer_" + 2));
        executorService.submit(new Tasks(props, TOPIC, "consumer_" + 3));
    }
}


class Tasks implements Runnable {
    private final Properties props;
    private final String topic;
    private final String consumerName;
    private volatile boolean closing = false;

    public Tasks(Properties props, String topic, String consumerName) {
        this.props = props;
        this.topic = topic;
        this.consumerName = consumerName;
    }

    @Override
    public void run() {
        try (KafkaConsumer<String, CustomerAvro> consumer = new KafkaConsumer<>(props)) {
            Runtime.getRuntime().addShutdownHook(
                    new Thread(() -> {
                        closing = true;
                        System.out.println("consumer is closed");
                    })
            );

            consumer.subscribe(Collections.singletonList(topic));

            Map<TopicPartition, OffsetAndMetadata> offsets = new HashMap<>();
            int count = 0;

            while (!closing) {
                ConsumerRecords<String, CustomerAvro> records = consumer.poll(Duration.ofMillis(400));
                for (ConsumerRecord<String, CustomerAvro> record : records) {
                    CustomerAvro customer = record.value();
                    System.out.printf("[consumerName=%s], offset=%d, customer=%s \n", consumerName, record.offset(), customer.toString());

                    offsets.put(
                            new TopicPartition(record.topic(), record.partition()),
                            new OffsetAndMetadata(record.offset() + 1, "no metadata")
                    );

                    if (count % 100 == 0) {
                        consumer.commitAsync(offsets, null);
                    }
                    count++;
                }
                consumer.commitAsync((Map<TopicPartition, OffsetAndMetadata> offset, Exception e) -> {
                    if (e != null) {
                        System.out.println("commit failed for offsets" + offset + ", e=" + e);
                    }
                });
            }
            try {
                consumer.commitSync();
            } catch (CommitFailedException fe) {
                System.out.println("e=" + fe);
            } finally {
                consumer.close();
            }
        }
    }
}

package schemaRegistryProducerConsumer;

import domain.avro.CustomerAvro;
import io.confluent.kafka.serializers.KafkaAvroSerializer;
import io.confluent.kafka.serializers.KafkaAvroSerializerConfig;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import utils.CustomerGenerator;

import java.util.List;
import java.util.Properties;

public class Producer {
    private static final int CUSTOMER_QUANTITY = 10000;
    private static final String TOPIC = "input";

    public static void main(String[] args) throws InterruptedException {
        Properties props = new Properties();
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:29092");
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, KafkaAvroSerializer.class.getName());
        props.put(KafkaAvroSerializerConfig.SCHEMA_REGISTRY_URL_CONFIG, "http://localhost:8081");

        try (KafkaProducer<String, CustomerAvro> producer = new KafkaProducer<>(props)) {

            Runtime.getRuntime().addShutdownHook(new Thread(producer::close));

            List<CustomerAvro> customers = CustomerGenerator.generate().limit(CUSTOMER_QUANTITY).toList();

            for (CustomerAvro customer : customers) {
                Thread.sleep(200);
                ProducerRecord<String, CustomerAvro> record = new ProducerRecord<>(TOPIC, customer);
                producer.send(record, (recordMetadata, e) -> {
                    if (e != null) System.out.printf("error is occurred: %s \n", e.getMessage());
                    else System.out.printf("offset=%d, customer.ssid=%s \n", recordMetadata.offset(), customer.getSsid());
                } );
            }
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

    }
}
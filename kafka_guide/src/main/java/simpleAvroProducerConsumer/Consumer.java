package simpleAvroProducerConsumer;

import domain.avro.CustomerAvro;
import org.apache.kafka.clients.consumer.*;
import org.apache.kafka.common.TopicPartition;
import utils.PropertiesUtil;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.time.Duration;
import java.util.*;

public class Consumer {
    public static void main(String[] args) {
        Properties properties = PropertiesUtil.getKafkaConsumerProperties();
        Runnable consumerTask = new ConsumerRunner(properties, "output", "max3000");
        Runnable consumerTask2 = new ConsumerRunner(properties, "output", "max3000_2");
        Runnable consumerTask3 = new ConsumerRunner(properties, "output", "max3000_3");
        new Thread(consumerTask).start();
        new Thread(consumerTask2).start();
        new Thread(consumerTask3).start();
    }
}

class ConsumerRunner implements Runnable {
    private final Properties props;
    private final String topic;
    private final String consumerName;
    private final Map<TopicPartition, OffsetAndMetadata> partitionOffsetAndMetadataMap;

    public ConsumerRunner(Properties props, String topic, String consumerName) {
        this.props = props;
        this.topic = topic;
        this.consumerName = consumerName;
        partitionOffsetAndMetadataMap = new HashMap<>();
    }

    @Override
    public void run() {
        try (KafkaConsumer<String, byte[]> consumer = new KafkaConsumer<>(props)) {
            Runtime.getRuntime().addShutdownHook(new Thread(consumer::close));

            consumer.subscribe(List.of(topic), new ConsumerRebalanceListener() {
                @Override
                public void onPartitionsRevoked(Collection<TopicPartition> collection) {}

                @Override
                public void onPartitionsAssigned(Collection<TopicPartition> collection) {
                    consumer.commitSync(partitionOffsetAndMetadataMap);
                }
            });

            while (true) {
                ConsumerRecords<String, byte[]> records = consumer.poll(Duration.ofMillis(100));
                for (ConsumerRecord<String, byte[]> record : records) {
                    Thread.sleep(400);
                    CustomerAvro customer = CustomerAvro.fromByteBuffer(ByteBuffer.wrap(record.value()));
                    String output = String.format("thread=%s, key=%s, offset=%d, partition=%d, customer.name=%s, customer=%s",
                            consumerName, record.key(), record.offset(), record.partition(), customer.getFullName(), customer.toString());
                    System.out.println(output);
                    partitionOffsetAndMetadataMap.put(
                            new TopicPartition(record.topic(), record.partition()),
                            new OffsetAndMetadata(record.offset() + 1, "no metadata")
                    );
                }
                consumer.commitAsync(partitionOffsetAndMetadataMap, null);

            }
        } catch (IOException | InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}

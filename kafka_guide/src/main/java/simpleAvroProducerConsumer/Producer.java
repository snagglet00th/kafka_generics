package simpleAvroProducerConsumer;

import domain.avro.CustomerAvro;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import utils.CustomerGenerator;
import utils.PropertiesUtil;

import java.io.IOException;
import java.util.List;
import java.util.Properties;


public class Producer {
    private final static String TOPIC = "output";
    private final static int CUSTOMER_QUANTITY = 10000;

    public static void main(String[] args) {
        Properties conf = PropertiesUtil.getKafkaProducerProperties();
        ProducerRunner producerRunner = new ProducerRunner(conf, TOPIC, "first thread");
        Thread thread = new Thread(producerRunner);
        thread.start();

    }
}

class ProducerRunner implements Runnable {
    private final Properties conf;
    private final String topic;
    private final String producerName;

    public ProducerRunner(Properties conf, String topic, String producerThreadName) {
        this.conf = conf;
        this.topic = topic;
        this.producerName = producerThreadName;
    }

    @Override
    public void run() {
        try (KafkaProducer<String, byte[]> producer = new KafkaProducer<>(conf)) {
            Runtime.getRuntime().addShutdownHook(new Thread(producer::close));
            List<CustomerAvro> customers = CustomerGenerator.generate().limit(20000).toList();
            for (CustomerAvro customer : customers) {
                Thread.sleep(200);
                byte[] customerB = customer.toByteBuffer().array();
                ProducerRecord<String, byte[]> record = new ProducerRecord<>(topic, customer.getSsid().toString(), customerB);
                System.out.printf("producer=%s sent record=%s \n", producerName, record.key());
                producer.send(record);
            }
        } catch (IOException | InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}


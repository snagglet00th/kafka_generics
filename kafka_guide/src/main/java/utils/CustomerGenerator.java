package utils;

import com.github.javafaker.Faker;
import domain.avro.Address;
import domain.avro.CustomerAvro;

import java.util.UUID;
import java.util.stream.Stream;

public class CustomerGenerator {
    public static Stream<CustomerAvro> generate() {
        Faker stub = new Faker();
        return Stream.generate(() -> CustomerAvro.newBuilder()
                .setSsid(UUID.randomUUID().toString())
                .setFullName(stub.name().fullName())
                .setIsAdmin(stub.bool().bool())
                .setAddress(
                    Address.newBuilder()
                        .setCountry(stub.address().country())
                        .setCity(stub.address().cityName())
                        .setStreet(stub.address().streetAddress())
                        .setZip(stub.address().zipCode())
                        .build()
                )
                .build());
    }

}

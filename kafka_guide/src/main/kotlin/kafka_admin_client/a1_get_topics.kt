package kafka_admin_client.kafka_admin_client

import mu.KotlinLogging
import org.apache.kafka.clients.admin.AdminClient
import org.apache.kafka.clients.admin.AdminClientConfig
import org.apache.kafka.clients.admin.ListTopicsResult
import java.util.*

private val log = KotlinLogging.logger("a1_topics")

fun main() {
    val prop = Properties()
    prop[AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG] = "localhost:29092"
    val admin = AdminClient.create(prop)

    val topics: ListTopicsResult = admin.listTopics()
    topics.names().get().forEach { topic -> log.info { topic } }

    admin.close()
}
package kafka_admin_client.kafka_admin_client

import org.apache.kafka.clients.admin.AdminClient
import org.apache.kafka.clients.admin.AdminClientConfig
import org.apache.kafka.clients.admin.ListOffsetsResult
import org.apache.kafka.clients.admin.OffsetSpec
import org.apache.kafka.clients.consumer.OffsetAndMetadata
import org.apache.kafka.common.TopicPartition
import java.util.*


fun main() {
    val props = Properties()
    props[AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG] = "localhost:29092"
    val admin = AdminClient.create(props)

    val offsets: MutableMap<TopicPartition, OffsetAndMetadata> = admin.listConsumerGroupOffsets("superTeam")
        .partitionsToOffsetAndMetadata().get()

    val requestLatestOffsets = mutableMapOf<TopicPartition, OffsetSpec>()

    for (topicPartition: TopicPartition in offsets.keys) {
        requestLatestOffsets[topicPartition] = OffsetSpec.latest()
    }

    val latestOffsets: MutableMap<TopicPartition, ListOffsetsResult.ListOffsetsResultInfo> = admin.listOffsets(requestLatestOffsets).all().get()

    for (e: MutableMap.MutableEntry<TopicPartition, ListOffsetsResult.ListOffsetsResultInfo> in latestOffsets.entries) {
        val topic = e.key.topic()
        val partition = e.key.partition()
        val committedOffset = e.value.offset()
        val latestOffset = latestOffsets[e.key]?.offset()

        println("")
    }
}
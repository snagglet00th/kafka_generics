package kafka_admin_client.kafka_admin_client

import org.apache.kafka.clients.admin.AdminClient
import org.apache.kafka.clients.admin.AdminClientConfig
import java.util.Properties


fun main() {
    val props = Properties()
    props[AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG] = "localhost:29092"
    val admin = AdminClient.create(props)

    admin.listConsumerGroups().valid().get().forEach { println(it) }
}
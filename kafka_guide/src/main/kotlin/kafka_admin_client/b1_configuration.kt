package kafka_admin_client.kafka_admin_client

import org.apache.kafka.clients.admin.AdminClient
import org.apache.kafka.clients.admin.AdminClientConfig
import org.apache.kafka.clients.admin.Config
import org.apache.kafka.common.config.ConfigResource
import java.util.Properties


fun main() {
    val props = Properties()
    props[AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG] = "localhost:29092"
    val admin = AdminClient.create(props)

    val configResource = ConfigResource(ConfigResource.Type.TOPIC, "output")
    val describeConfigsResult = admin.describeConfigs(listOf(configResource))
    val configs: Config? = describeConfigsResult.all().get()[configResource]

    println("<<<<<<<<<<<<<<<<<<<<<<<<")
    if (configs == null) throw NullPointerException("config is null")
    configs.entries().stream()
//        .filter { entry -> !entry.isDefault }
        .forEach { println("name=${it.name()}, val=${it.value()}, $it") }
    println(">>>>>>>>>>>>>>>>>>>>>>>>")
}
package kafka_admin_client.kafka_admin_client

import org.apache.kafka.clients.admin.*
import java.util.*


fun main() {
    val consumerGroup = "superTeam"
    val props = Properties()
    props[AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG] = "localhost:29092"
    val admin = AdminClient.create(props)

    val groupDescription = admin.describeConsumerGroups(listOf(consumerGroup))
        .describedGroups()[consumerGroup]?.get()

    println("description of group $consumerGroup : $groupDescription")
}

fun consFun() {
    val conf = Properties()
    conf[AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG] = "localhost:29092"

    val admin = AdminClient.create(conf)
    val describeConsumerGroups: DescribeConsumerGroupsResult = admin.describeConsumerGroups(Collections.singleton("superTeam"))
    val groupDescription: ConsumerGroupDescription? = describeConsumerGroups.describedGroups()["superTeam"]?.get()
    val members: MutableCollection<MemberDescription> = groupDescription!!.members()
    val memberFunc: (MemberDescription) -> Unit = { member -> println("${member.assignment()}, ${member.clientId()}, ${member.host()}, ${member.consumerId()}") }
    members.forEach(memberFunc)
}
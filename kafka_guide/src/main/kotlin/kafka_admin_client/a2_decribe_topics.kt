package kafka_admin_client.kafka_admin_client

import mu.KotlinLogging
import org.apache.kafka.clients.admin.*
import java.util.*


private val log = KotlinLogging.logger("a2_desc_topics")

fun main() {
    val CHECK_TOPIC = "new_super_topic"
    val props = Properties()
    props[AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG] = "localhost:29092"
    val admin = AdminClient.create(props)

    val demoTopics: DescribeTopicsResult = admin.describeTopics(listOf("output", "input"))
    admin.use { a ->
        val topicDesc: MutableMap<String, TopicDescription> = demoTopics.allTopicNames().get()
        println(">>>>>>>>>>>>>>>>>>>>>>>>>")
        topicDesc.forEach { (key, topicDesc) -> log.info { "key=${key}, topicDesc=${topicDesc}" } }
        println("<<<<<<<<<<<<<<<<<<<<<<<<<")

        val listTopics = admin.listTopics()
        listTopics.names().get().forEach { println(it) }
        val targetTopicDesc = demoTopics.allTopicNames().get()[CHECK_TOPIC]
        if (targetTopicDesc == null) {
            log.warn { "topic $CHECK_TOPIC does not exist. Going to create it now" }
            createTopic(a, CHECK_TOPIC, 5, 1)
        }
    }
}

fun createTopic(admin: AdminClient, topicName: String, numPartitions: Int, repFactor: Short): CreateTopicsResult {
    return admin.createTopics(
        listOf(
            NewTopic(topicName, numPartitions, repFactor)
        )
    )
}

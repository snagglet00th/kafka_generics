package kafka_admin_client.kafka_admin_client

import org.apache.kafka.clients.admin.*
import org.apache.kafka.common.config.ConfigResource
import org.apache.kafka.common.config.TopicConfig
import java.util.*


fun main() {
    val topic = "output"

    val props = Properties()
    props[AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG] = "localhost:29092"
    val client = AdminClient.create(props)

    val configResource = ConfigResource(ConfigResource.Type.TOPIC, topic)

    val describeConfigs = client.describeConfigs(Collections.singleton(configResource))
    val config: Config? = describeConfigs.all().get()[configResource]

    val configEntry = ConfigEntry(TopicConfig.CLEANUP_POLICY_CONFIG, TopicConfig.CLEANUP_POLICY_COMPACT)
    if (!config!!.entries().contains(configEntry)) {
        val alterConfigOp = AlterConfigOp(configEntry, AlterConfigOp.OpType.SET)
        val alterConfigMap: MutableMap<ConfigResource, Collection<AlterConfigOp>> = mutableMapOf()

        alterConfigMap[configResource] = Collections.singleton(alterConfigOp)
        client.incrementalAlterConfigs(alterConfigMap).all().get()
    } else {
        println("Topic $topic is compacted")
    }
}
